package com.connectto.zimadtest.view.adapter.viewHolders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.connectto.zimadtest.model.Item
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_layout.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class ItemViewHolder constructor(view: View, private val lambda: (View, Item?, Int) -> Unit) :
    RecyclerView.ViewHolder(view) {


    fun configHolder(item: Item?, position: Int) {

        Picasso.get().load(item?.url).into(itemView.imageview)
        itemView.number.text = position.toString()
        itemView.title.text = item?.title

        itemView.onClick {
            lambda.invoke(itemView, item, position)
        }
    }

}