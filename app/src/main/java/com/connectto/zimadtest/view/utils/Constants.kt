package com.connectto.zimadtest.view.utils

class Constants {
    companion object {
        const val BASE_URL = "https://kot3.com/"
        const val REQUEST_TIMEOUT_DURATION = 10
        const val DATA_ITEM = "data"
        const val NUMBER = "number"
        const val DEBUG = true
    }
}