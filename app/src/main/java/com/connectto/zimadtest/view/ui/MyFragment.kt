package com.connectto.zimadtest.view.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.connectto.zimadtest.R
import com.connectto.zimadtest.databinding.FragmentViewBinding
import com.connectto.zimadtest.model.Item
import com.connectto.zimadtest.view.adapter.MyAdapter
import com.connectto.zimadtest.view.model.FragmentViewModel
import com.connectto.zimadtest.view.utils.Constants
import kotlinx.android.synthetic.main.fragment_view.*
import kotlinx.android.synthetic.main.item_layout.view.*

class MyFragment : Fragment() {

    private lateinit var viewDataBinding: FragmentViewBinding
    private lateinit var adapter: MyAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = FragmentViewBinding.inflate(inflater, container, false).apply {
            fragmentViewModel =
                ViewModelProvider(this@MyFragment).get(FragmentViewModel::class.java)
            setLifecycleOwner(viewLifecycleOwner)
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding.fragmentViewModel?.fetchItemList(getSearchKey())

        setupAdapter()
        setupObservers()
    }

    private fun getSearchKey(): String {
        return if (!arguments?.getString("state").isNullOrBlank()) arguments?.getString("state")!!
        else getString(R.string.search_cat)
    }

    private fun setupObservers() {
        viewDataBinding.fragmentViewModel?.itemListLive?.observe(viewLifecycleOwner, Observer {
            adapter.updateList(it)
        })
    }

    private fun setupAdapter() {
        val viewModel = viewDataBinding.fragmentViewModel
        if (viewModel != null) {
            adapter = MyAdapter(lamda)
            val layoutManager = LinearLayoutManager(activity)
            recycler_view.layoutManager = layoutManager
            recycler_view.addItemDecoration(
                DividerItemDecoration(
                    activity,
                    layoutManager.orientation
                )
            )
            recycler_view.adapter = adapter
        }
    }

    private fun sharedIntent(itemView: View, item: Item?, position: Int) {
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            activity!!,
            Pair.create<View, String>(itemView.imageview, getString(R.string.transition_image)),
            Pair.create<View, String>(itemView.number, getString(R.string.transition_number)),
            Pair.create<View, String>(itemView.title, getString(R.string.transition_title))
        )

        val intent = Intent(activity, SharedElementActivity::class.java)

        intent.putExtra(Constants.DATA_ITEM, item)
        intent.putExtra(Constants.NUMBER, position)

        startActivity(intent, options.toBundle())
    }

    private var lamda: (View, Item?, Int) -> Unit =
        { view: View, item: Item?, pos: Int -> sharedIntent(view, item, pos) }
}