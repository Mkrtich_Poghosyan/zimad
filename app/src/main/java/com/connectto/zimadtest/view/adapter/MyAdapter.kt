package com.connectto.zimadtest.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.connectto.zimadtest.R
import com.connectto.zimadtest.model.Item
import com.connectto.zimadtest.view.adapter.viewHolders.ItemViewHolder

class MyAdapter(private val lambda: (View, Item?, Int) -> Unit) :
    RecyclerView.Adapter<ItemViewHolder>() {


    var mList: List<Item?>? = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_layout,
                parent,
                false
            ), lambda
        )
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.configHolder(mList?.get(position), position)
    }

    fun updateList(mList: List<Item?>?) {
        this.mList = mList
        notifyDataSetChanged()
    }
}