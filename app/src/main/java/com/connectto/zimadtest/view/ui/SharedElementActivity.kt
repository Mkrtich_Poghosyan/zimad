package com.connectto.zimadtest.view.ui

import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import com.connectto.zimadtest.R
import com.connectto.zimadtest.model.Item
import com.connectto.zimadtest.view.utils.Constants
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.shared_element_activity.*

class SharedElementActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.shared_element_activity)

        val item: Item = intent?.getSerializableExtra(Constants.DATA_ITEM) as Item

        Picasso.get().load(item.url).into(shared_image)
        shared_number.text = intent?.getIntExtra(Constants.NUMBER, 0).toString()
        shared_title.text = item.title
    }

    override fun onSupportNavigateUp(): Boolean {
        finishAfterTransition()
        return true
    }
}