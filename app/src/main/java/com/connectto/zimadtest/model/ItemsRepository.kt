package com.connectto.zimadtest.model

import com.connectto.zimadtest.model.api.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ItemsRepository {

    fun getItemList(
        search: String,
        onResult: (isSuccess: Boolean, response: ResponseObject?) -> Unit
    ) {

        ApiClient.instance.getItems(search).enqueue(object : Callback<ResponseObject> {
            override fun onResponse(
                call: Call<ResponseObject>?,
                response: Response<ResponseObject>?
            ) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }

            override fun onFailure(call: Call<ResponseObject>?, t: Throwable?) {
                onResult(false, null)
            }
        })
    }

    companion object {
        private var INSTANCE: ItemsRepository? = null
        fun getInstance() = INSTANCE
            ?: ItemsRepository().also {
                INSTANCE = it
            }
    }
}