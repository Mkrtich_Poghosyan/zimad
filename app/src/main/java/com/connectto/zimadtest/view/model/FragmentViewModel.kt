package com.connectto.zimadtest.view.model

import androidx.lifecycle.MutableLiveData
import com.connectto.zimadtest.model.Item
import com.connectto.zimadtest.model.ItemsRepository
import com.connectto.zimadtest.view.base.BaseViewModel

class FragmentViewModel : BaseViewModel() {

    val itemListLive = MutableLiveData<List<Item?>?>()
    val scrollPosition = 0;
    fun fetchItemList(value: String) {
        dataLoading.value = true
        ItemsRepository.getInstance().getItemList(value) { isSuccess, response ->
            dataLoading.value = false
            if (isSuccess) {
                itemListLive.value = response?.data
                empty.value = false
            } else {
                empty.value = true
            }
        }
    }

}