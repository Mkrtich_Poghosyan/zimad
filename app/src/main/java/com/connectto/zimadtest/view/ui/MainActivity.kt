package com.connectto.zimadtest.view.ui


import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.connectto.zimadtest.R
import com.connectto.zimadtest.view.utils.NetworkUtils
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), TabLayout.OnTabSelectedListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tab_layout.addOnTabSelectedListener(this)

        if (!NetworkUtils.isInternetAvailable(this)) {
            showNoInternet()
        }
    }

    private fun showNoInternet() {
        Toast.makeText(this, getString(R.string.no_inernet_msg), Toast.LENGTH_LONG).show()
    }

    override fun onTabReselected(p0: TabLayout.Tab?) {

    }

    override fun onTabUnselected(p0: TabLayout.Tab?) {

    }

    override fun onTabSelected(p0: TabLayout.Tab?) {
        if (p0!!.position == 0) {
            findNavController(R.id.main_nav_fragment).navigate(R.id.tab1)
        } else {
            findNavController(R.id.main_nav_fragment).navigate(R.id.tab2)
        }
    }


}
