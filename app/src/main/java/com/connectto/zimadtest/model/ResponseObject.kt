package com.connectto.zimadtest.model

import java.io.Serializable

data class ResponseObject(
    val message: String?,
    val data: List<Item?>?
)

data class Item(
    val url: String?,
    val title: String?
) : Serializable