package com.connectto.zimadtest.model.api

import com.connectto.zimadtest.model.ResponseObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("xim/api.php?")
    fun getItems(@Query("query") search: String): Call<ResponseObject>
}